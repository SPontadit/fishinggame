﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class FollowCamera : MonoBehaviour {

    private bool _isActive = false;
    public bool IsActive
    {
        get { return _isActive; }
        set { _isActive = value;}
    }

    public float velocity;
    public Transform target;
    Vector3 posCam;
    Vector3 targetZ;
    Vector3 positionFinal;
    float posX;

    private Action Follow;
    public Action IsReady;

    // Use this for initialization
    void Start()
    {
        Follow = FirstFollow;
        posCam = transform.position;
        posX = posCam.x;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Follow();
    }

    void FirstFollow()
    {
        if (target && _isActive)
        {
            targetZ = transform.position;
            targetZ.z = target.transform.position.z;
            Vector3 targetDirection = (target.transform.position - targetZ);

            float interpVelocity = targetDirection.magnitude * velocity;

            posCam = transform.position + (targetDirection.normalized * interpVelocity * Time.deltaTime);

            positionFinal = Vector3.Lerp(transform.position, posCam, 0.02f);
            positionFinal.x = posX;
            transform.position = positionFinal;

            if ((transform.position.y - target.position.y) < 0.6f)
            {
                Follow = FishingFollow;
                if (IsReady != null)
                    IsReady();
            }
        }
    }

    void FishingFollow()
    {
        if (target)
        {
            targetZ = transform.position;
            targetZ.z = target.transform.position.z;
            Vector3 targetDirection = (target.transform.position - targetZ);

            float interpVelocity = targetDirection.magnitude * velocity;

            posCam = transform.position + (targetDirection.normalized * interpVelocity * Time.deltaTime);

            positionFinal = Vector3.Lerp(transform.position, posCam, 200f);
            positionFinal.x = posX;
            transform.position = positionFinal;
        }
    }
}
