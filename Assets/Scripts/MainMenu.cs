﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {

    private Button _launchGame;
    private Button _options;
    private Button _quit;

    private bool _isRegister;

    void Awake()
    {
        _isRegister = false;
        Transform buttons = transform.FindChild("Buttons");

        _launchGame = buttons.FindChild("LaunchGame").GetComponent<Button>();
        _options = buttons.FindChild("Options").GetComponent<Button>();
        _quit = buttons.FindChild("Quit").GetComponent<Button>();

        RegisterFunction();
    }

    void RegisterFunction()
    {
        _launchGame.onClick.AddListener(() =>
        {
            // LaunchGame
            print("Launch");
        });

        _options.onClick.AddListener(() =>
        {
            // Options
        });

        _quit.onClick.AddListener(() =>
        {
            // Quit
            Application.Quit();
        });
    }
}
