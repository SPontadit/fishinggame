﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviorManager : MonoBehaviour {

    private int _currentScore = 0;
    private bool _isCoroutineLaunched = false;
    private Vector3 _lastPosition = Vector3.zero;

    [SerializeField]
    private float _moveTolerance;

    public void BasicMovement(Fish fish)
    {
        if (Time.time > fish.NextMove)
        {
            fish.NextMove = Time.time + fish.moveRate;
            fish.SetTargetMove();
        }
        float step = fish.speed * Time.deltaTime;
        fish.transform.position = Vector3.MoveTowards(fish.transform.position, fish.Target, step);
    }

    public void PassiveBait(Fish fish)
    {
        if (!_isCoroutineLaunched)
        {
            StartCoroutine(CalculateBaitScore(fish));
            _lastPosition = fish.Player.transform.position;
        }

        if ((_lastPosition - fish.Player.transform.position).magnitude == 0f)
        {
            _currentScore++;
        }
        else
        {
            if (_currentScore > 0)
                _currentScore--;
        }

        _lastPosition = fish.Player.transform.position;
    }

    public void OffensiveBait(Fish fish)
    {
        if (!_isCoroutineLaunched)
        {
            StartCoroutine(CalculateBaitScore(fish));
            _lastPosition = fish.Player.transform.position;
        }


        if ((_lastPosition - fish.Player.transform.position).magnitude != 0f)
        {
            _currentScore++;
        }
        else
        {
            if (_currentScore > 0)
                _currentScore--;
        }

        _lastPosition = fish.Player.transform.position;
    }

    public void BasicFight(Fish fish)
    {

    }

    private IEnumerator CalculateBaitScore(Fish fish)
    {
        _isCoroutineLaunched = true;
        yield return new WaitForSeconds(fish.BaitTime);

        if (_currentScore >= fish.BaitScore)
            fish.CurrentState = Fish.STATE.STATE_FIGHT;
        else
            fish.CurrentState = Fish.STATE.STATE_MOVE;

        _currentScore = 0;
        _isCoroutineLaunched = false;
    }
}
