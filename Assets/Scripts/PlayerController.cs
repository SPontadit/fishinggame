﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {


    private InputManager inputManager;
    private Rigidbody2D rigideBody;
    private bool isRegister;
    private bool lineIsStop;

    void Awake()
    {
        isRegister = false;
        lineIsStop = false;
        inputManager = InputManager.Instance;
        RegisterInputFunction();
    }

    void Start()
    {
        rigideBody = GetComponent<Rigidbody2D>();
    }

    void OnDestroy()
    {
        UnregisterInputFunction();
    }

    void RegisterInputFunction()
    {
        if (!isRegister)
        {
            inputManager.rightClick += ThrowFishingLine;
            inputManager.space += StopLine;
            inputManager.keyRight += MoveRight;
            inputManager.keyLeft += MoveLeft;
            inputManager.keyUp += MoveUp;

            isRegister = true;
        }
    }

    void UnregisterInputFunction()
    {
        if (isRegister)
        {
            inputManager.rightClick -= ThrowFishingLine;
            inputManager.space -= StopLine;
            inputManager.keyRight -= MoveRight;
            inputManager.keyLeft -= MoveLeft;
            inputManager.keyUp -= MoveUp;

            isRegister = false;
        }
    }


    // Input Function
    void ThrowFishingLine()
    {
        inputManager.State = InputManager.INPUT_STATE.INPUT_FISHING;
        Camera.main.GetComponent<FollowCamera>().IsActive = true;
        Camera.main.GetComponent<FollowCamera>().IsReady += EnablePhysic;
    }

    void EnablePhysic()
    {
        rigideBody.simulated = true;
    }

    void StopLine()
    {
        if (!lineIsStop)
        {
            rigideBody.constraints = RigidbodyConstraints2D.FreezePositionY;
        }
        else
        {
            rigideBody.constraints = RigidbodyConstraints2D.None;
        }

        lineIsStop = !lineIsStop;
    }

    void MoveRight()
    {
        rigideBody.AddForce(Vector2.right / 5, ForceMode2D.Impulse);
    }

    void MoveLeft()
    {
        rigideBody.AddForce(Vector2.left / 5, ForceMode2D.Impulse);
    }

    void MoveUp()
    {
        if (lineIsStop)
            rigideBody.constraints = RigidbodyConstraints2D.None;

        rigideBody.AddForce(Vector2.up / 5, ForceMode2D.Impulse);
    }
}
