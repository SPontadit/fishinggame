﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Fish : MonoBehaviour {

   public enum BAIT_TYPE
    {
        TYPE_PASSIVE,
        TYPE_OFFENSIVE,
        TYPE_COUNT
    }

    public enum MOVE_TYPE
    {
        TYPE_DEFAULT
    }

    public enum FIGHT_TYPE
    {
        TYPE_DEFAULT
    }

    public enum STATE
    {
        STATE_MOVE,
        STATE_BAIT,
        STATE_FIGHT
    }


    public bool defaultValue = true;
    
    [Header("Caracteristiques")]
    [SerializeField]
    private int _score;
    [SerializeField]
    private int _baitScore;
    [SerializeField]
    private int _depth;
    [SerializeField]
    private float _endurance;
   
    public int BaitScore
    {
        get { return _baitScore; }
    }

    private SpriteRenderer sprite;


    [Header("Move Settings")]
    public float moveRate = 5.0f;
    [SerializeField]
    private float _direction;
    private float _nextMove = 0.0f;
    public float NextMove
    {
        get { return _nextMove; }
        set { _nextMove = value; }
    }

    [SerializeField]
    private float _baitTime = 2f;
    public float BaitTime
    {
        get { return _baitTime; }
    }

    public float speed;
    public float minMovement = 1.0f;
    public float maxMovement = 10.0f;
    private Vector3 _target;
    public Vector3 Target
    {
        get { return _target; }
    }



    [Header("Behavior")]
    [SerializeField]
    private BAIT_TYPE _baitType;
    [SerializeField]
    private MOVE_TYPE _moveType;
    [SerializeField]
    private FIGHT_TYPE _fightType;

    private SpriteRenderer _sprite;

    private STATE _currentState;
    public STATE CurrentState
    {
        set { _currentState = value; UpdateCurrentState(); }
    }

    private void UpdateCurrentState()
    {
        switch(_currentState)
        {
            case STATE.STATE_MOVE:
            {
                StateUpdate = Move;
            }
            break;

            case STATE.STATE_BAIT:
            {
                StateUpdate = Bait;
            }
            break;

            case STATE.STATE_FIGHT:
            {
                StateUpdate = Fight;
            }
            break;
        }
    }

    private Action<Fish> Move;
    private Action<Fish> Bait;
    private Action<Fish> Fight;
    private Action<Fish> StateUpdate;

    private PlayerController _player;
    public PlayerController Player
    {
        get { return _player; }
    }

    void Start()
    {
        if (defaultValue)
            SetDefaultValue();

        _sprite = GetComponent<SpriteRenderer>();

        InitializeBehavior();

        StateUpdate = Move;

        _direction = transform.rotation.y;
        _target = new Vector3(0.0f, 0.0f, 0.0f);

        _player = FindObjectOfType<PlayerController>();
    }

    private void InitializeBehavior()
    {
        BehaviorManager behavior = FindObjectOfType<BehaviorManager>();

        InitializeBaitFunction(behavior);
        InitializeMoveFunction(behavior);
        InitializeFightFunction(behavior);
    }

    private void InitializeMoveFunction(BehaviorManager behavior)
    {
        switch (_moveType)
        {
            case MOVE_TYPE.TYPE_DEFAULT:
            {
                Move = behavior.BasicMovement;
            }
            break;

            default:
                break;
        }
    }

    private void InitializeBaitFunction(BehaviorManager behavior)
    {
        switch (_baitType)
        {
            case BAIT_TYPE.TYPE_PASSIVE:
            {
                Bait = behavior.PassiveBait;
            }
            break;

            case BAIT_TYPE.TYPE_OFFENSIVE:
            {
                Bait = behavior.OffensiveBait;
            }
                break;

            default:
                break;
        }
    }

    private void InitializeFightFunction(BehaviorManager behavior)
    {
        switch (_fightType)
        {
            case FIGHT_TYPE.TYPE_DEFAULT:
            {
                Fight = behavior.BasicFight;
            }
            break;

            default:
                break;
        }
    }

    void SetDefaultValue()
    {
        speed = 5.0f;
        _endurance = 10.0f;
        _depth = 0;
        _score = 10;
        _baitScore = 10;
        _baitType = BAIT_TYPE.TYPE_PASSIVE;
    }
   
    void Update () {
      
        // WARNING NULLREF ICI ?
          StateUpdate(this);

        //if (Time.time > _NextMove)
        //{
        //    _NextMove = Time.time + moveRate;
        //    SetTargetMove(); 
        //}
        //float step = speed * Time.deltaTime;
        //transform.position = Vector3.MoveTowards(transform.position, _target, step);
    }

    void SetRandomDirection()
    {
        gameObject.transform.rotation = Quaternion.LookRotation(-transform.forward, Vector3.up);
        _direction = transform.rotation.y;
    }

    public void SetTargetMove()
    {
        float randomDistance = 0.0f;

        if (_direction == 1.0f)
            randomDistance = UnityEngine.Random.Range(minMovement, maxMovement);
        else
            randomDistance = UnityEngine.Random.Range(-minMovement, -maxMovement);

        _target = transform.position;
        _target.x = transform.position.x + randomDistance;
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.GetComponent<PlayerController>() != null && _currentState != STATE.STATE_FIGHT)
        {
            print("HAMECON");
            CurrentState = STATE.STATE_BAIT;
        }
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Ground")
        {
            SetRandomDirection();
            SetTargetMove();
        }
    }

    void OnDrawGizmos()
    {
        switch (_currentState)
        {
            case STATE.STATE_MOVE:
            {
                Gizmos.color = Color.green;
            }
            break;

            case STATE.STATE_BAIT:
            {
                Gizmos.color = Color.blue;
            }
            break;

            case STATE.STATE_FIGHT:
            {
                Gizmos.color = Color.red;
            }
            break;
        }

        float higherScale = Mathf.Max(transform.localScale.x, transform.localScale.y, transform.localScale.z);
        Gizmos.DrawSphere(transform.position, 0.5f);
    }
}
