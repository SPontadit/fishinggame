﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class InputManager : MonoBehaviour {

    private static InputManager instance;
    public static InputManager Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType<InputManager>();

            return instance;
        }
    }

    public enum INPUT_STATE
    {
        INPUT_MENU,
        INPUT_SURFACE,
        INPUT_FISHING,
        INPUT_FIGHT
    }

    private INPUT_STATE _state;
    public INPUT_STATE State
    {
        get { return _state; }
        set { _state = value; ChangeInputMode(); }
    }

    private Action InputCheck;

    public delegate void InputKey();
    public event InputKey rightClick;
    public event InputKey keyRight;
    public event InputKey keyLeft;
    public event InputKey keyUp;
    public event InputKey space;


    void Start()
    {
        State = INPUT_STATE.INPUT_SURFACE;
        ChangeInputMode();
    }

    void Update()
    {
        CheckGeneralInput();
        InputCheck();
    }

    void ChangeInputMode()
    {
        switch(State)
        {
            case INPUT_STATE.INPUT_MENU:
            {
                    InputCheck = CheckInputMenu;
            }
            break;

            case INPUT_STATE.INPUT_SURFACE:
            {
                    InputCheck = CheckInputSurface;
            }
            break;

            case INPUT_STATE.INPUT_FISHING:
            {
                    InputCheck = CheckInputFishing;
            }
            break;

            case INPUT_STATE.INPUT_FIGHT:
            {
                    InputCheck = CheckInputFight;
            }
            break;

            default:
                break;
        }
    }

    void CheckGeneralInput()
    {

        if (Input.GetButtonDown("NextInput"))
        {
            print("WERWERWERWERW");
            if (State == INPUT_STATE.INPUT_MENU)
            {
                State = INPUT_STATE.INPUT_SURFACE;
                print("InputMode : SURFACE");
            }
            else if (State == INPUT_STATE.INPUT_SURFACE)
            {
                State = INPUT_STATE.INPUT_FISHING;
                print("InputMode : FISHING");
            }
            else if (State == INPUT_STATE.INPUT_FISHING)
            {
                State = INPUT_STATE.INPUT_FIGHT;
                print("InputMode : FIGHT");
            }
            else if (State == INPUT_STATE.INPUT_FIGHT)
            {
                State = INPUT_STATE.INPUT_MENU;
                print("InputMode : MENU");
            }
        }
    }

    void CheckInputMenu()
    {

    }

    void CheckInputSurface()
    {
        if (Input.GetButtonDown("Throw") && rightClick != null)
        {
            rightClick();
        }
    }

    void CheckInputFishing()
    {
        if(Input.GetButtonDown("Right")  && keyRight != null)
        {
            keyRight();
        }
        else if (Input.GetButtonDown("Left")  && keyLeft != null)
        {
            keyLeft();
        }

        if (Input.GetButtonDown("Up")  && keyUp != null)
        {
            keyUp();
        }

        if (Input.GetButtonDown("StopLine")  && space != null)
        {
            space();
        }
    }

    void CheckInputFight()
    {
        if (Input.GetButtonDown("Right")  && keyRight != null)
        {
            keyRight();
        }
        else if (Input.GetButtonDown("Left")  && keyLeft != null)
        {
            keyLeft();
        }

        if (Input.GetButtonDown("Up")  && keyUp != null)
        {
            keyUp();
        }
    }
    
}
