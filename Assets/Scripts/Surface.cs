﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Surface : MonoBehaviour {


    private Button _playerMenu;
    private Button _shopMenu;

    void Start () {
        _playerMenu = transform.FindChild("PlayerMenu").GetComponent<Button>();
        _shopMenu = transform.FindChild("ShopMenu").GetComponent<Button>();

        RegisterFunction();
    }

    void RegisterFunction()
    {
        _playerMenu.onClick.AddListener(() =>
       {
           print("PlayerMenu");
       });

        _shopMenu.onClick.AddListener(() =>
        {
            print("ShopMenu");
        });
    }

}
