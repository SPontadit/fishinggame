﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class LevelManager : MonoBehaviour {

    private static LevelManager instance;
    public static LevelManager Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType<LevelManager>();

            return instance;
        }
    }

    [SerializeField]
    private int enemyCount = 2;
    private Cellule[] cellule;
    private GameObject _fishPrefab;

    void Awake()
    {
        _fishPrefab = Resources.Load<GameObject>("Prefabs/Fish");
        SceneManager.sceneLoaded += InstantiateSpawners;
    }

    void InstantiateSpawners(Scene scene, LoadSceneMode mode)
    {
        GameObject level = GameObject.Find("Level");

        int subdivisionLevel = 2;

        if (level != null)
        {
            int celluleIndex = 1;
            float celluleSize = 10f;

            foreach (Cellule cellule in level.GetComponentsInChildren<Cellule>())
            {
                Transform spawners = cellule.transform.Find("Spawners");

                for (int i = 0; i < enemyCount; ++i)
                {
                    GameObject spawner = Instantiate(_fishPrefab);
                    Vector2 position = ComputeRandomSpawnPosition(i, subdivisionLevel);
                    position.y -= celluleIndex * celluleSize;
                    spawner.transform.position = position;
                    spawner.transform.rotation = Quaternion.identity;
                    spawner.transform.SetParent(spawner.transform);
                }
                ++celluleIndex;
            }
        }
    }

    Vector3 ComputeRandomSpawnPosition(int index, int subdivisionLevel)
    {
        float rangeX = 14f;
        float rangeY = 7f;

        Vector3 position;

        float tileSizeX = rangeX / subdivisionLevel;
        float tileSizeY = rangeY / subdivisionLevel;
        position.x = UnityEngine.Random.Range(-rangeX/2 + index * tileSizeX, -rangeX/2 + (index + 1) * tileSizeX);
        position.y = UnityEngine.Random.Range(-rangeY/2 + index * tileSizeY, -rangeY/2 + (index + 1) * tileSizeY);
        position.z = 0f;

        return position;
    }
}
